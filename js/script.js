var game = new Phaser.Game(360, 200, Phaser.CANVAS, '', { init: init, preload: preload, create: create, update: update, render : render }, false, false, null);

var pixel = {scale: 2, canvas: null, context: null, width: 0, height: 0 };

function init() {
    game.canvas.style['display'] = 'none';
    pixel.canvas = Phaser.Canvas.create(game.width * pixel.scale, game.height * pixel.scale);
    pixel.context = pixel.canvas.getContext('2d');
    Phaser.Canvas.addToDOM(pixel.canvas);
    Phaser.Canvas.setSmoothingEnabled(pixel.context, false);
    pixel.width = pixel.canvas.width;
    pixel.height = pixel.canvas.height;
}

function preload() {
    game.load.image('ground', 'assets/ground.png');
    game.load.spritesheet('dude', 'assets/ninja.png', 18, 16);
}

var player;
var ground;
var cursors;
var scoreText;

var ascending = false;
var descending = false;
var jumpFactor = 90;

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);

    ground = game.add.sprite(0, game.world.height / 2, 'ground');

    player = game.add.sprite(50, game.world.height / 3, 'dude');
    game.physics.arcade.enable(player);

    player.body.collideWorldBounds = true;

    player.animations.add('left', [3, 4], 5, true);
    player.animations.add('right', [1, 2], 5, true);

    cursors = game.input.keyboard.createCursorKeys();

    scoreText = game.add.text(4, 4, 'FPS : 0', { fontSize: '8px', fill: '#FFF' });
    game.time.advancedTiming = true;
}

function update() {
    player.body.velocity.x = 0;

    scoreText.text = 'FPS : ' + game.time.fps;

    if (ascending || descending) {
        if (ascending) {
        player.body.velocity.y = - (1 / (Math.pow(jumpFactor, 3)));
        jumpFactor -= 0.2;
            if (jumpFactor < 0) {
                ascending = false;
                descending = true;
            }
        } else if (descending) {
            player.body.velocity.y = 1 / (Math.pow(jumpFactor, 3));
            jumpFactor += 0.2;
            if (jumpFactor >= 9) {
                ascending = false;
                descending = false;
                jumpFactor = 9;
            }
        }
    }
    if (ascending) {
        player.body.velocity.y = -(Math.pow(jumpFactor, 3));
        jumpFactor -= 0.2;
        if (jumpFactor < 0) {
            ascending = false;
            descending = true;
        }
    } else if (descending) {
        player.body.velocity.y = -(Math.pow(jumpFactor, 3));
        jumpFactor += 0.2;
        if (jumpFactor >= 5) {
            ascending = false;
            descending = false;
        }
    }

    if (cursors.left.isDown) {
        player.body.velocity.x = -80;
        player.animations.play('left');
    } else if (cursors.right.isDown) {
        player.body.velocity.x = 80;
        player.animations.play('right');
    }

    if (cursors.up.isDown) {
        player.body.velocity.y = -50;
        player.animations.play('right');
    } else if (cursors.down.isDown) {
        player.body.velocity.y = 50;
        player.animations.play('right');
    }

    if (!cursors.up.isDown &&
        !cursors.down.isDown &&
        !cursors.left.isDown &&
        !cursors.right.isDown)
    {
        player.animations.stop();
        player.frame = 0;
    }

    if (game.input.keyboard.isDown(32))
    {
        ascending = true;
    }
}

function render() {
    pixel.context.drawImage(game.canvas, 0, 0, game.width, game.height, 0, 0, pixel.width, pixel.height);
}