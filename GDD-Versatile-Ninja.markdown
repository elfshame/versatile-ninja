The historical truth behind the "mystical" ninja warriors is shrouded in mystery, but our team of historians and archaeologists can, hopefully, offer you some insight into the daily life of a real ninja.

Medieval Japan, 1996

Lord Mochiron has been captured by evil ninjas, and you seem to have misplaced your swords!
Arrow keys to move, space to make do with what you have on hand

Lord Mochiron & Lady Chigau

Weight classes
Size classes
Object category
    Animals 4 large 4 medium 4 small
    Household items 4 large 4 medium 4 small
    Metals & minerals 4 large 4 medium 4 small
    Food 4 large 4 medium 4 small
    Plants & vegetables 4 large 4 medium 4 small
    Famous people 4 large 4 medium 4 small
    Elements 4 large 4 medium 4 small
    Literature 4 large 4 medium 4 small
    Sports items 4 large 4 medium 4 small
