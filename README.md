#Versatile Ninja

##An abandoned LD32 project
Welcome to Versatile Ninja.

###What is this?
I tried to make Versatile Ninja using Phaser.js for Ludum Dare 32, but ended up pulling my hair out in frustration. Simply put, making the game involved so many hurdles that I lost patience and quit over the weekend.

###Why do it now?
Because it might not take very long to finish. We'll see. And it's supposed to be a pretty fun game, I'd like to be able to play it. Plus, it's good experience.

###What is the first objective?
Make a simple prototype that showcases how the game will be fun. No final assets yet.

More on that later.